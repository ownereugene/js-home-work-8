//1) Найти все параграфы на странице и установить цвет фона #ff0000

let tagParElements = document.getElementsByTagName("p");
for (let value of tagParElements) {
  value.style.backgroundColor = "#ff0000";
}
//2) Найти элемент с id="optionsList". Вывести в консоль.
//Найти родительский элемент и вывести в консоль.
//Найти дочерние ноды, если они есть, и вывести в консоль названия и тип нод.

let element = document.getElementById("optionsList");
console.log(element);
let idElement = element.parentElement;
console.log(idElement);
let elementNode = element.childNodes;
for (let value of elementNode) {
  console.log(value, typeof value);
}

// 3) Установите в качестве контента элемента с классом testParagraph следующий параграф <p>This is a paragraph</p>

let newParagraph = document.createElement('Paragraph');
let testParagraph = document.getElementById('testParagraph');
testParagraph.append(newParagraph);
newParagraph.innerHTML = "This is a paragraph";


// 4) Получить элементы <li>, вложенные в элемент с классом main-header и вывеcти их в консоль. Каждому из элементов присвоить новый класс nav-item.

let mainHeaderElements = document.getElementsByClassName("main-header");
console.log(mainHeaderElements);
for (let mainHeader of mainHeaderElements) {
    mainHeader.classList.add("nav-item");

}

// 5) Найти все элементы с классом section-title. Удалить этот класс у элементов.

let sectionTitleElements = document.getElementsByClassName("section-title");
for (let sectionTitle of sectionTitleElements) {
    sectionTitle.classList.remove("section-title");
}

